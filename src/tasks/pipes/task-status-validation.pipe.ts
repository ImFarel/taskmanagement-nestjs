import { PipeTransform, BadRequestException } from '@nestjs/common';
import { TaskStatus } from 'src/tasks/task-status.enum';

export class TaskStatusValidationPipe implements PipeTransform {
  readonly allowedstatuses = [
    TaskStatus.OPEN,
    TaskStatus.IN_PROGRESS,
    TaskStatus.DONE,
  ];

  transform(value: any) {
    value = value.toUpperCase();

    if (!this.isStatusInvalid(value)) {
      throw new BadRequestException(`${value} is an invalid status`);
    }

    return value;
  }

  isStatusInvalid(status: any) {
    const index = this.allowedstatuses.indexOf(status);
    return index !== -1;
  }
}
